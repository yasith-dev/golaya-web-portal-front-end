import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { IoMdArrowRoundForward } from 'react-icons/io';
import { GoDashboard } from 'react-icons/go';
import { HiDocumentDownload } from 'react-icons/hi';
import { AiOutlineStock } from 'react-icons/ai';
import { FiShoppingCart } from 'react-icons/fi';
import { BsList, BsListUl, BsBarChartFill } from 'react-icons/bs';

import '../../style/navigation.css'

function Nav(props) {
    const [dashboardStyle, setDashboardStyle] = useState();
    const [salesStyle, setSalesStyle] = useState();
    const [salesItemStyle, setSalesItemStyle] = useState();
    const [purchasingStyle, setPurchasingStyle] = useState();
    const [stockStyle, setStockStyle] = useState();
    const [discountStyle, setDiscountStyle] = useState();

    useEffect(() => {
        if (props.route == "dashboard") {
            focusDashboard();
        } else if (props.route == "sales") {
            focusSales();
        } else if (props.route == "sales-item") {
            focusSalesItem();
        } else if (props.route == "purchasing") {
            focusPurchasing();
        } else if (props.route == "stock") {
            focusStock();
        } else if (props.route == "discount") {
            focusDiscount();
        }
    }, []);

    const focusDashboard = () => {
        setDashboardStyle({
            "border-right": "5px solid rgb(80, 167, 218)",
            "color": "rgb(80, 167, 218)"
        });
        setSalesStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setSalesItemStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setPurchasingStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setStockStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setDiscountStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
    }

    const focusSales = () => {
        setDashboardStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setSalesStyle({
            "border-right": "5px solid rgb(80, 167, 218)",
            "color": "rgb(80, 167, 218)"
        });
        setSalesItemStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setPurchasingStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setStockStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setDiscountStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
    }

    const focusSalesItem = () => {
        setDashboardStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setSalesStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setSalesItemStyle({
            "border-right": "5px solid rgb(80, 167, 218)",
            "color": "rgb(80, 167, 218)"
        });
        setPurchasingStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setStockStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setDiscountStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
    }

    const focusPurchasing = () => {
        setDashboardStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setSalesStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setSalesItemStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setPurchasingStyle({
            "border-right": "5px solid rgb(80, 167, 218)",
            "color": "rgb(80, 167, 218)"
        });
        setStockStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setDiscountStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
    }

    const focusStock = () => {
        setDashboardStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setSalesStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setSalesItemStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setPurchasingStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setStockStyle({
            "border-right": "5px solid rgb(80, 167, 218)",
            "color": "rgb(80, 167, 218)"
        });
        setDiscountStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
    }

    const focusDiscount = () => {
        setDashboardStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setSalesStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setSalesItemStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setPurchasingStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setStockStyle({
            "color": "#f9faf7b9",
            "transition": "color 500ms"
        });
        setDiscountStyle({
            "border-right": "5px solid rgb(80, 167, 218)",
            "color": "rgb(80, 167, 218)"
        });
    }

    const toggleHover = (tab, hover) => {
        if (tab == "dashboard") {
            if (props.route != "dashboard") {
                if (hover) {
                    setDashboardStyle({
                        "border-right": "5px solid #f9faf75d",
                        "transition": "color 500ms, border-right 1s",
                        "color": "rgb(255, 255, 255)"
                    });
                } else {
                    setDashboardStyle({
                        "border-right": "5px solid rgb(32, 33, 34)",
                        "color": "#f9faf7b9",
                        "transition": "color 500ms, border-right 1s"
                    });
                }
            }
        } else if (tab == "sales") {
            if (props.route != "sales") {
                if (hover) {
                    setSalesStyle({
                        "border-right": "5px solid #f9faf75d",
                        "transition": "color 500ms, border-right 1s",
                        "color": "rgb(255, 255, 255)"
                    });
                } else {
                    setSalesStyle({
                        "border-right": "5px solid rgb(32, 33, 34)",
                        "color": "#f9faf7b9",
                        "transition": "color 500ms, border-right 1s"
                    });
                }
            }
        } else if (tab == "salesItem") {
            if (props.route != "sales-item") {
                if (hover) {
                    setSalesItemStyle({
                        "border-right": "5px solid #f9faf75d",
                        "transition": "color 500ms, border-right 1s",
                        "color": "rgb(255, 255, 255)"
                    });
                } else {
                    setSalesItemStyle({
                        "border-right": "5px solid rgb(32, 33, 34)",
                        "color": "#f9faf7b9",
                        "transition": "color 500ms, border-right 1s"
                    });
                }
            }
        } else if (tab == "purchasing") {
            if (props.route != "purchasing") {
                if (hover) {
                    setPurchasingStyle({
                        "border-right": "5px solid #f9faf75d",
                        "transition": "color 500ms, border-right 1s",
                        "color": "rgb(255, 255, 255)"
                    });
                } else {
                    setPurchasingStyle({
                        "border-right": "5px solid rgb(32, 33, 34)",
                        "color": "#f9faf7b9",
                        "transition": "color 500ms, border-right 1s"
                    });
                }
            }
        } else if (tab == "stock") {
            if (props.route != "stock") {
                if (hover) {
                    setStockStyle({
                        "border-right": "5px solid #f9faf75d",
                        "transition": "color 500ms, border-right 1s",
                        "color": "rgb(255, 255, 255)"
                    });
                } else {
                    setStockStyle({
                        "border-right": "5px solid rgb(32, 33, 34)",
                        "color": "#f9faf7b9",
                        "transition": "color 500ms, border-right 1s"
                    });
                }
            }
        } else {
            if (props.route != "discount") {
                if (hover) {
                    setDiscountStyle({
                        "border-right": "5px solid #f9faf75d",
                        "transition": "color 500ms, border-right 1s",
                        "color": "rgb(255, 255, 255)"
                    });
                } else {
                    setDiscountStyle({
                        "border-right": "5px solid rgb(32, 33, 34)",
                        "color": "#f9faf7b9",
                        "transition": "color 500ms, border-right 1s"
                    });
                }
            }
        }
    }

    return (
        <div id="nav-main">
            <div id="nav-main-list-icon"><BsList  style={{ marginTop: '15px', color: 'rgb(198, 211, 230)', fontSize: '30px' }} /></div>
            <Link to="/dashboard" className="nav-main-tabs" style={dashboardStyle} onClick={e => focusDashboard()} onMouseEnter={e => toggleHover("dashboard", true)} onMouseLeave={e => toggleHover("dashboard", false)}>
                <div className="main-tab-icons"><GoDashboard className="icons" /></div>
                <div className="main-tab-names">DASHBOARD</div>
            </Link>
            <Link to="/sales" className="nav-main-tabs" style={salesStyle} onClick={e => focusSales()} onMouseEnter={e => toggleHover("sales", true)} onMouseLeave={e => toggleHover("sales", false)}>
                <div className="main-tab-icons"><BsBarChartFill className="icons" /></div>
                <div className="main-tab-names">SALES REPORT</div>
            </Link>
            <Link to="/sales-item" className="nav-main-tabs" style={salesItemStyle} onClick={e => focusSalesItem()} onMouseEnter={e => toggleHover("salesItem", true)} onMouseLeave={e => toggleHover("salesItem", false)}>
                <div className="main-tab-icons"><BsListUl className="icons" /></div>
                <div className="main-tab-names">SALES ITEM REPORT</div>
            </Link>
            <Link to="/purchasing" className="nav-main-tabs" style={purchasingStyle} onClick={e => focusPurchasing()} onMouseEnter={e => toggleHover("purchasing", true)} onMouseLeave={e => toggleHover("purchasing", false)}>   
                <div className="main-tab-icons"><FiShoppingCart className="icons" /></div>
                <div className="main-tab-names">PURCHASING REPORT</div>
            </Link>
            <Link to="/stock" className="nav-main-tabs" style={stockStyle} onClick={e => focusStock()} onMouseEnter={e => toggleHover("stock", true)} onMouseLeave={e => toggleHover("stock", false)}>
                <div className="main-tab-icons"><AiOutlineStock className="icons" /></div>
                <div className="main-tab-names">STOCK REPORT</div>
            </Link>
            <Link to="/discount" className="nav-main-tabs" style={discountStyle} onClick={e => focusDiscount()} onMouseEnter={e => toggleHover("discount", true)} onMouseLeave={e => toggleHover("discount", false)}>
                <div className="main-tab-icons"><HiDocumentDownload className="icons" /></div>
                <div className="main-tab-names">DISCOUNT REPORT</div>
            </Link>
        </div>
    );
}

export default Nav;
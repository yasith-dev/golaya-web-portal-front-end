import React, { useState, useEffect, useRef } from 'react';
import { TextField, Button } from '@material-ui/core';
import Modal from 'react-modal';
import CryptoJS from 'crypto-js';
import { useHistory, Link } from 'react-router-dom';

import '../../style/login.css'

function Login() {
    const [username, setUsername] = useState("");
    const usernameRef = useRef(null);
    const [password, setPassword] = useState("");
    const passwordRef = useRef(null);
    const [showLoginAlertModal, setShowLoginAlertModal] = useState(false);
    const [showRegisterAlertModal, setShowRegisterAlertModal] = useState(false);
    const [modalStyle, setModalStyle] = useState({overlay: {zIndex: 2}});
    const [modalContent, setModalContent] = useState("");
    let history = useHistory();
    const okRef = useRef(null);
    const [regUsername, setRegUsername] = useState("");
    const regUsernameRef = useRef(null);
    const [regPassword, setRegPassword] = useState("");
    const regPasswordRef = useRef(null);

    useEffect(() => {
        if (localStorage.getItem("jt") !== null) {
            history.push("/dashboard")
        }
        usernameRef.current.focus();
    }, []);

    const login = async () => {
        if (username != "" && password != "") {
            const response = await fetch('auth/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "username": username,
                    "password": password
                })
            });
            const responseObj = await response.json();

            if (typeof responseObj["token"] != "undefined") {
                // Encrypt
                var ciphertext = CryptoJS.AES.encrypt(responseObj["token"], '53KR3TKE').toString();
                localStorage.setItem('jt', ciphertext);
                localStorage.setItem('user', username);
                history.push("/dashboard")
            } else {
                setModalContent(
                    <>
                    <div style={{ height: '60px', paddingTop: '15px', fontSize: '18px' }}>{responseObj["error"]}</div>
                    <Button onClick={e => closeModal(0)} variant="contained" style={{ marginTop: '20px' }}>OK</Button>
                    </>
                );
                openModal(0);
            }

        } else {
            setModalContent(
                <>
                <div style={{ height: '50px', paddingTop: '25px', fontSize: '18px' }}>Please enter your login details!</div>
                <Button onClick={e => closeModal(0)} variant="contained" style={{ marginTop: '20px' }}>OK</Button>
                </>
            );
            openModal(0);
        }
    }

    const reset = () => {
        setUsername("");
        setPassword("");
        setRegUsername("");
        setRegPassword("");
    }

    const openModal = type => {
        if (type === 0) {
            setShowLoginAlertModal(true);
        } else {
            setShowRegisterAlertModal(true);
        }
        
    }

    const closeModal = type => {
        if (type === 0) {
            setShowLoginAlertModal(false);
        } else {
            setShowRegisterAlertModal(false);
        }
        
    }

    const regLogin = () => {
        if (regUsername != "" && regPassword != "") {
            if (regUsername == "admin" && regPassword == "adminceymoss1234") {
                localStorage.setItem('reg', regUsername);
                history.push('/register');
            } else {
                alert("Incorrect Username or Password! Try again.");
                regUsernameRef.current.focus();
            }
        } else {
            alert("Please enter details to proceed!")
            regUsernameRef.current.focus();
        }
    }

    return (
        <>
        <div style={{ position: 'fixed', height: '100%', width: '100%', backgroundColor: 'white' }}>
            <div id="frame">
                <table id="login-content">
                    <tr><TextField className="user-details" id="outlined-basic" label="Username" inputRef={usernameRef} onKeyDown={e => {if (e.keyCode === 13) {passwordRef.current.focus();}}} value={username} onChange={e => setUsername(e.target.value)} variant="outlined" /></tr>
                    <tr><TextField className="user-details" id="outlined-basic" label="Password" inputRef={passwordRef} onKeyDown={e => {if (e.keyCode === 13) {login();}}} type="password" value={password} onChange={e => setPassword(e.target.value)} variant="outlined" /></tr>
                    <tr>
                        <Button onClick={e => login()} variant="contained" color="primary" style={{ float: 'left', marginRight: '20px', width: '100px' }}>LOGIN</Button>
                        <Button onClick={e => reset()} variant="contained" style={{ float: 'left', width: '100px' }}>RESET</Button>
                    </tr>
                    <tr style={{ fontSize: '18px' }}><Link onClick={e => openModal(1)}>Register User</Link></tr>
                </table>
            </div>
        </div>
        <Modal style={ modalStyle } id="login-alert-modal" isOpen={showLoginAlertModal} closeOnDocumentClick onRequestClose={e => closeModal(0)}>
            <div>
                {modalContent}
            </div>
        </Modal>
        <Modal style={ modalStyle } id="register-alert-modal" isOpen={showRegisterAlertModal} closeOnDocumentClick onRequestClose={e => closeModal(1)}>
            <table id="reg-login-content">
                <tr><TextField className="reg-user-details" id="outlined-basic" label="Username" inputRef={regUsernameRef} onKeyDown={e => {if (e.keyCode === 13) {regPasswordRef.current.focus();}}} value={regUsername} onChange={e => setRegUsername(e.target.value)} variant="outlined" /></tr>
                <tr><TextField className="reg-user-details" id="outlined-basic" label="Password" inputRef={regPasswordRef} onKeyDown={e => {if (e.keyCode === 13) {regLogin();}}} type="password" value={regPassword} onChange={e => setRegPassword(e.target.value)} variant="outlined" /></tr>
                <tr>
                    <Button onClick={e => regLogin()} variant="contained" color="primary" style={{ float: 'left', marginRight: '20px', width: '100px' }}>PROCEED</Button>
                    <Button onClick={e => reset()} variant="contained" style={{ float: 'left', width: '100px' }}>RESET</Button>
                </tr>
            </table>
        </Modal>
        </>
    );
}

export default Login;
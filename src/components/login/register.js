import React, { useState, useEffect, useRef } from 'react';
import Modal from 'react-modal';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Tooltip from '@material-ui/core/Tooltip';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom';
import { UsersTable } from './tables';

import '../../style/login.css';
import 'bootstrap/dist/css/bootstrap.css';

function Register() {
    const [name, setName] = useState("");
    const nameRef = useRef(null);
    const [lastname, setLastname] = useState("");
    const [nic, setNic] = useState("");
    const [gender, setGender] = useState("");
    const [address, setAddress] = useState("");
    const [contactNo, setContactNo] = useState("");
    const [username, setUsername] = useState("");
    const [usernameAlert, setUsernameAlert] = useState("");
    const [password, setPassword] = useState("");
    const [passwordConfirm, setPasswordConfirm] = useState("");
    const [email, setEmail] = useState("");
    const [location, setLocation] = useState("");
    const [locationOpts, setLocationOpts] = useState("");
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [modalContent, setModalContent] = useState();
    const [users, setUsers] = useState();
    const [authUsername, setAuthUsername] = useState("");
    const [authPassword, setAuthPassword] = useState("");
    const [id, setId] = useState("");
    let history = useHistory();

    useEffect(() => {
        if (localStorage.getItem("reg") === null) {
            history.push("/login");
            return;
        }

        getUsers();
        getLocations();
        nameRef.current.focus();
    }, []);

    useEffect(() => {
        setPasswordConfirm("");
    }, [password])

    useEffect(() => {
        if (id != "") {
            authenticateDelete(id);
        }
    }, [id])

    const getUsers = async () => {
        const response = await fetch('/users/list', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const responseObj = await response.json();

        if (responseObj.length > 0) {
            let userList = [];
            for (let i=0; i<responseObj.length; i++) {
                userList.push(<tr><td>{responseObj[i]["name"]}</td><td>{responseObj[i]["username"]}</td><td>{responseObj[i]["mobile"]}</td><td>{responseObj[i]["user_type"]}</td><td><button onClick={e => deleteAuthentication(responseObj[i]["id"])} className="tbl-delete-btns">DELETE</button></td></tr>);
            }
            setUsers(userList);
        }
    }

    const deleteAuthentication = id => {
        setModalContent(
            <table id="reg-login-content">
                <tr><td><TextField className="reg-user-details" id="outlined-basic" label="Username" variant="outlined" onChange={e => setAuthUsername(e.target.value)} /></td></tr>
                <tr><td><TextField type="password" className="reg-user-details" id="outlined-basic" label="Password" variant="outlined" onChange={e => setAuthPassword(e.target.value)} /></td></tr>
                <tr><td><Button onClick={e => setId(id)} variant="contained" color="primary" style={{ float: 'left', marginRight: '20px', width: '100px' }}>DELETE</Button></td></tr>
            </table>
        );
        openDeleteModal();
    }

    const openDeleteModal = () => {
        setShowDeleteModal(true);
    }

    const closeDeleteModal = () => {
        setShowDeleteModal(false);
        setId("");
    }

    const authenticateDelete = id => {
        if (authUsername != "" && authPassword != "") {
            if (authUsername == "admin" && authPassword == "adminceymoss1234") {
                proceedDelete(id);
            } else {
                alert("Incorrect Username or Password! Try again.");
            }
        } else {
            alert("Please fill in the details!");
        }
        closeDeleteModal();
    }

    const proceedDelete = async id => {
        const response = await fetch('/users/status?id='+id, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const responseObj = await response.text();
        if (responseObj) {
            closeDeleteModal();
            getUsers();
        } else {
            alert(responseObj);
        }    
    }

    const getLocations = async e => {
        const response = await fetch('/users/location', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const responseObj = await response.json();
        if (responseObj[0]["id"] > 0) {
            const locationOptList = responseObj.map((element, key) => (
                <MenuItem selected={location === element["name"]} value={element["id"]}>{element["name"]}</MenuItem>
            ));
            setLocationOpts(locationOptList);
        } else {
            alert(responseObj)
        }
    }

    const checkUsernameAvailability = async e => {
        let userName = e.target.value;
        if (userName != "") {
            const response = await fetch('/users/username/availability?username='+userName, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            const responseMessage = await response.text();
            if (responseMessage == "true") {
                setUsernameAlert("Username available!");
            } else {
                setUsernameAlert(<div>&#10004;</div>);
            }
        } else {
            setUsernameAlert("");
        }
    }

    const confirmPassword = val => {
        if (val != password) {
            setPasswordConfirm("");
        }
    }

    const resetForm = () => {
        setName("");
        setLastname("");
        setNic("");
        setGender("");
        setAddress("");
        setContactNo("");
        setUsername("");
        setUsernameAlert("");
        setPassword("");
        setPasswordConfirm("");
        setEmail("");
        setLocation("");
        
        nameRef.current.focus();
    }

    const create = async () => {   
        if (name != "" && lastname != "" && nic != "" && gender != "" && username != "" && usernameAlert != "Username available!" && password != "" && passwordConfirm != "" && email != "" && location != "") {  
            const response = await fetch('/users', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "name": name,
                    "lastname": lastname,
                    "nic": nic,
                    "gender": gender,
                    "address": address,
                    "contactNo": contactNo,
                    "username": username,
                    "password": password,
                    "email": email,
                    "location": location
                })
            });
            const responseMessage = await response.json();
            if (responseMessage == 1) {
                alert("User created succesfully!");
                getUsers();
                resetForm();
            } else {
                alert(responseMessage);
            }
        } else {
            alert("Please fill out details!");
        }
    };

    const dirLogin = () => {
        localStorage.removeItem('reg');
        history.push('/login');
    }

    return (
        <>
        <div className="register-body">
            <button type="button" id="login-btn" onClick={e => dirLogin()}>Login</button>
            <div id="create-user-form">
                <div className="cu-headings">Users</div>
                <UsersTable rows={users} />
                <div className="cu-headings">User Details</div>
                <div className="cu-details">
                    <table style={{ width: '940px' }}>
                        <tr>
                            <td style={{ width: '100px' }}>Name<Tooltip title="required" arrow placement="right" className="required-mark"><span>*</span></Tooltip></td>
                            <td style={{ width: '200px' }}><input type="text" value={name} ref={nameRef} onChange={e => setName(e.target.value)} /></td>
                            <td style={{ width: '200px' }}></td>
                            <td style={{ width: '170px' }}>Last Name<Tooltip title="required" arrow placement="right" className="required-mark"><span>*</span></Tooltip></td>
                            <td style={{ width: '250px' }}><input type="text" value={lastname} onChange={e => setLastname(e.target.value)} /></td>
                        </tr>
                        <tr>
                            <td>NIC No<Tooltip title="required" arrow placement="right" className="required-mark"><span>*</span></Tooltip></td>
                            <td><input type="text" value={nic} onChange={e => setNic(e.target.value)} /></td>
                        </tr>
                        <tr>
                            <td>Gender<Tooltip title="required" arrow placement="right" className="required-mark"><span>*</span></Tooltip></td>
                            <td>
                                <Select value={gender} onChange={e => setGender(e.target.value)} className="select">
                                    <MenuItem selected={gender === "MALE"} value="MALE">Male</MenuItem>
                                    <MenuItem selected={gender === "FEMALE"} value="FEMALE">Female</MenuItem>
                                </Select>
                            </td>
                        </tr>
                        <tr>
                            <td>Contact No</td>
                            <td><input type="text" value={contactNo} onChange={e => setContactNo(e.target.value)} /></td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td><input type="text" value={address} onChange={e => setAddress(e.target.value)} /></td>
                        </tr>
                    </table>
                </div>
                <div className="cu-headings" style={{ marginTop: '40px' }}>Account Details</div>
                <div className="cu-details">
                    <table style={{ width: '940px' }}>
                        <tr>
                            <td>Username<Tooltip title="required" arrow placement="right" className="required-mark"><span>*</span></Tooltip></td>
                            <td><input type="text" value={username} onChange={e => setUsername(e.target.value)} onBlur={e => checkUsernameAvailability(e)} /></td>
                            <td><div style={{marginLeft: 5, color: 'green', fontSize: '18px', float: 'left'}}>{usernameAlert}</div></td>
                        </tr>
                        <tr>
                            <td style={{ width: '100px' }}>Password<Tooltip title="required" arrow placement="right" className="required-mark"><span>*</span></Tooltip></td>
                            <td style={{ width: '200px' }}><input type="password" value={password} onChange={e => setPassword(e.target.value)} /></td>
                            <td style={{ width: '200px' }}></td>
                            <td style={{ width: '170px' }}>Confirm Password<Tooltip title="required" arrow placement="right" className="required-mark"><span>*</span></Tooltip></td>
                            <td style={{ width: '250px' }}><input type="password" value={passwordConfirm} onChange={e => setPasswordConfirm(e.target.value)} onBlur={e => confirmPassword(e.target.value)} /></td>
                        </tr>
                        <tr>
                            <td>Email<Tooltip title="required" arrow placement="right" className="required-mark"><span>*</span></Tooltip></td>
                            <td><input type="text" value={email} onChange={e => setEmail(e.target.value)} /></td>
                        </tr>
                        <tr>
                            <td>Location<Tooltip title="required" arrow placement="right" className="required-mark"><span>*</span></Tooltip></td>
                            <td>
                                <Select value={location} onChange={e => setLocation(e.target.value)} className="select">
                                    {locationOpts}
                                </Select>
                            </td>
                        </tr>
                    </table>
                </div>
                <button type="button" onClick={e => create()} id="cu-save-btn">REGISTER</button><button type="button" onClick={e => resetForm()} id="cu-reset-btn" tabIndex={-1}>RESET</button>
            </div>
        </div>
        <Modal id="register-alert-modal" isOpen={showDeleteModal} closeOnDocumentClick onRequestClose={e => closeDeleteModal()}>
            {modalContent}
        </Modal>
        </>
    );
}

export default Register;
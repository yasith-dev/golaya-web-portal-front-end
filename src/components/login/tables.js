import React, { useState, useEffect } from 'react';

export function UsersTable(props) {
    return (
        <div id="users-tbl-wrapper">
            <table id="users-tbl">
                <thead><tr><th>Name</th><th>Username</th><th>Contact No</th><th>User Type</th><th></th></tr></thead>
                <tbody>{props.rows}</tbody>
            </table>
        </div>
);
}
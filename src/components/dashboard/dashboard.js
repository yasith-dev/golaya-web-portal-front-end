import React, { useState, useEffect } from 'react';

import '../../style/dashboard.css';

function Dashboard(props) {
    return (
        <div id="dashboard">
            <div id="line-graph-container">{props.lineGraph}</div>
        </div>
    );
}

export default Dashboard;
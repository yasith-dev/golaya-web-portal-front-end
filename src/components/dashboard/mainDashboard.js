import React, { useState, useEffect } from 'react';
import { IoIosLogOut } from 'react-icons/io';
import { BiLeftArrow, BiRightArrow } from 'react-icons/bi';
import { Link } from 'react-router-dom';
import { Line, Doughnut } from 'react-chartjs-2';
import { hexToRgb } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import CryptoJS from 'crypto-js';
import { subDays, subMonths, subYears, addDays, addMonths, addYears } from 'date-fns';

import '../../style/dashboard.css';

function MainDashboard() {
    const [day, setDay] = useState();
    const [dayDate, setDayDate] = useState(new Date());
    const [footerDate, setFooterDate] = useState("");
    const [month, setMonth] = useState();
    const [dayMonth, setDayMonth] = useState(new Date());
    const [footerMonth, setFooterMonth] = useState("");
    const [year, setYear] = useState();
    const [dayYear, setDayYear] = useState(new Date());
    const [footerYear, setFooterYear] = useState();
    const [proceedNext, setProceedNext] = useState(false);
    const [salesDChart, setSalesDChart] = useState();
    const [topItems, setTopItem] = useState();
    const [topCategories, setTopCategories] = useState();
    var history = useHistory();

    useEffect(() => {
        if (localStorage.getItem("jt") === null) {
            history.push("/login")
            return;
        }

        window.addEventListener('scroll', e => {
            if (window.scrollY > 10) {
                document.getElementsByClassName("heading-bar")[0].style.boxShadow = "0px 0px 5px 0px rgb(196, 196, 196)";
            } else {
                document.getElementsByClassName("heading-bar")[0].style.boxShadow = "0px 0px 0px 0px rgb(196, 196, 196)";
            }
        });

        getSalesData();
    }, []);

    useEffect(() => {
        getDaySales(dayDate);
    }, [dayDate]);

    useEffect(() => {
        getMonthSales(dayMonth, proceedNext);
    }, [dayMonth]);

    useEffect(() => {
        getYearSales(dayYear, proceedNext);
    }, [dayYear]);

    const getSalesData = async () => {
        let date = subDays(new Date(), 1)
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        const response = await fetch('sales/data?date='+date.toISOString().slice(0, 10), {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();

        if (responseObj["sales"].length > 0) {
            let cost = responseObj["sales"][0]["total_cost"];
            let sales = responseObj["sales"][0]["total_sales"]
            let profit = sales - cost;

            let labelArray = ["Cost", "Profit"];
            let dataArray = [];
            if (cost == null) {
                dataArray.push(0);
                dataArray.push(0);
            } else {
                dataArray = [cost.toFixed(2), profit.toFixed(2)]
            }
            
            generateDChart(labelArray, dataArray);
        }

        let items = [];
        if (responseObj["items"].length > 0) {
            for (let i=0; i<responseObj["items"].length; i++) {
                items.push(<tr><td style={{ textAlign: 'left', paddingLeft: '50px' }}>{responseObj["items"][i]["item_name"]}</td><td style={{ textAlign: 'right', paddingRight: '50px' }}>{parseFloat(responseObj["items"][i]["qty"].toFixed(3))}</td></tr>);
            }
        } else {
            items.push(<tr><td style={{ textAlign: 'left', paddingLeft: '50px' }}>No Items</td><td style={{ textAlign: 'right', paddingRight: '50px' }}>-</td></tr>);
        }
        setTopItem(items);
        
        let categories = [];
        if (responseObj["categories"].length > 0) {
            for (let i=0; i<responseObj["categories"].length; i++) {
                categories.push(<tr><td style={{ textAlign: 'left', paddingLeft: '50px' }}>{responseObj["categories"][i]["category"]}</td><td style={{ textAlign: 'right', paddingRight: '50px' }}>{parseFloat(responseObj["categories"][i]["qty"].toFixed(3))}</td></tr>);
            }
        } else {
            categories.push(<tr><td style={{ textAlign: 'left', paddingLeft: '50px' }}>No Categories</td><td style={{ textAlign: 'right', paddingRight: '50px' }}>-</td></tr>)
        }
        setTopCategories(categories);

        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }
    }

    const generateDChart = (labelArray, dataArray) => {
        const data = {
            labels: labelArray,
            datasets: [{
                data: dataArray,
                backgroundColor: [
                '#63abeb',
                '#0bd692'
                ],
                hoverBackgroundColor: [
                    '#63abeb',
                    '#0bd692'
                ],
                hoverBorderWidth: 8,
            }]
        };

        const options = {   
            cutoutPercentage: 0,
            legend: {
                display: true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    precision: 2,
                    fontColor: function (data) {
                        var rgb = hexToRgb(data.dataset.backgroundColor[data.index]);
                        var threshold = 180;
                        var luminance = 0.299 * rgb.r + 0.587 * rgb.g + 0.114 * rgb.b;
                        return luminance > threshold ? 'black' : 'white';
                    },
                    fontSize: '16',
                    fontFamily: "Rubik"
                }
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var allData = data.datasets[tooltipItem.datasetIndex].data;
                        var tooltipLabel = data.labels[tooltipItem.index];
                        var tooltipData = allData[tooltipItem.index];
                        return tooltipLabel + ' ' + tooltipData;
                    }
                }
            }
        };

        setSalesDChart(
            <Doughnut
                data={data}
                options={options}
                height={205}
                id="d-chart-sales"
            />
        );
    }

    const getDaySales = async date => {
        setFooterDate(date.toDateString())
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);
        date = date.toISOString().slice(0, 10);

        const response = await fetch('sales/day?date='+date, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();

        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }

        let labelArray = [], salesArray = [], costsArray = [];
        for (let i=0; i<responseObj.length; i++) {
            labelArray.push(responseObj[i]["timestamp"].slice(12, 19));
            salesArray.push(responseObj[i]["sale"].toFixed(2));
            costsArray.push(responseObj[i]["cost"].toFixed(2));
        }
        generateLineGraph(labelArray, salesArray, costsArray, "day")
    }

    const getMonthSales = async (date, proceed) => {
        let from, to;
        if (proceed) {
            from = subMonths(date, 1).toISOString().slice(0, 10);
            to = date.toISOString().slice(0, 10);
        } else {
            from = subMonths(date, 1).toISOString().slice(0, 10);
            to = date.toISOString().slice(0, 10);
        }
        setFooterMonth(from + " to " + to);

        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        const response = await fetch('sales/month?from='+from+'&to='+to, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();

        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }

        let labelArray = [], salesArray = [], costsArray = [];
        for (let i=0; i<responseObj.length; i++) {
            labelArray.push(responseObj[i]["timestamp"].slice(0, 10));
            salesArray.push(responseObj[i]["sale"].toFixed(2));
            costsArray.push(responseObj[i]["cost"].toFixed(2));
        }
        generateLineGraph(labelArray, salesArray, costsArray, "month")
    }

    const getYearSales = async (date, proceed) => {
        let from, to;
        if (proceed) {
            from = subYears(date, 1).toISOString().slice(0, 10);
            to = date.toISOString().slice(0, 10);
        } else {
            from = subYears(date, 1).toISOString().slice(0, 10);
            to = date.toISOString().slice(0, 10);
        }
        setFooterYear(from + " to " + to);

        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        const response = await fetch('sales/year?from='+from+'&to='+to, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();

        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }

        let labelArray = [], salesArray = [], costsArray = [];
        for (let i=0; i<responseObj.length; i++) {
            labelArray.push(responseObj[i]["timestamp"].slice(0, 10));
            salesArray.push(responseObj[i]["sale"].toFixed(2));
            costsArray.push(responseObj[i]["cost"].toFixed(2));
        }
        generateLineGraph(labelArray, salesArray, costsArray, "year")
    }

    const generateLineGraph = (labelArray, salesArray, costsArray, type) => {
        let salesColor = "", costsColor = "";
        if (type == "day") {
            salesColor = "#f17e5d";
            costsColor = "#f2c96f";
        } else if (type == "month") {
            salesColor = "#f05b5b";
            costsColor = "#3293c0";
        } else {
            salesColor = "#06a6b8";
            costsColor = "#1bcfbc";
        }

        const chart = {
            data: (canvas) => {
              return {
                labels: labelArray,
                datasets: [
                    {
                        borderColor: costsColor,
                        backgroundColor: costsColor,
                        pointRadius: 0,
                        pointHoverRadius: 3,
                        borderWidth: 0,
                        data: costsArray,
                        label: "Costs"
                      },
                    {
                        borderColor: salesColor,
                        backgroundColor: salesColor,
                        pointRadius: 0,
                        pointHoverRadius: 3,
                        borderWidth: 0,
                        data: salesArray,
                        label: "Sales"
                    }
                ],
              };
            },
            options: {
              legend: {
                display: true
              },
              responsive: true,
          
              tooltips: {
                mode: 'index',
                intersect: false,
              },
              hover: {
                mode: 'nearest',
                intersect: true
             },
          
              scales: {
                yAxes: [
                    {
                        ticks: {
                          fontColor: "#9f9f9f",
                          beginAtZero: false,
                          maxTicksLimit: 5,
                          //padding: 20
                        },
                        gridLines: {
                          drawBorder: false,
                          zeroLineColor: "#ccc",
                          color: "rgba(255,255,255,0.05)",
                        },
                      }
                ],
          
                xAxes: [
                    {
                        ticks: {
                          fontColor: "#9f9f9f",
                          beginAtZero: false,
                          maxTicksLimit: 5,
                          //padding: 20
                        },
                        gridLines: {
                          drawBorder: false,
                          zeroLineColor: "#ccc",
                          color: "rgba(255,255,255,0.05)",
                        },
                      }
                ],
              },
            },
        };

        if (type == "day") {
            setDay(
                <Line
                    data={chart.data}
                    options={chart.options}
                    width={400}
                    height={110}
                    id="l-graph"
                />
            );
        } else if (type == "month") {
            setMonth(
                <Line
                    data={chart.data}
                    options={chart.options}
                    width={400}
                    height={110}
                    id="l-graph"
                />
            );
        } else {
            setYear(
                <Line
                    data={chart.data}
                    options={chart.options}
                    width={400}
                    height={110}
                    id="l-graph"
                />
            );
        }
    }

    const previous = type => {
        setProceedNext(false);
        let date;
        if (type == "day") {
            date = subDays(dayDate, 1)
            setDayDate(date);
        } else if (type == "month") {
            date = subMonths(dayMonth, 1)
            setDayMonth(date);
        } else {
            date = subYears(dayYear, 1)
            setDayYear(date);
        }
    }

    const next = type => {
        setProceedNext(true);
        let date;
        if (type == "day") {
            date = addDays(dayDate, 1);
            setDayDate(date);
        } else if (type == "month") {
            date = addMonths(dayMonth, 1);
            setDayMonth(date);
        } else {
            date = addYears(dayYear, 1);
            setDayYear(date);
        }
    }

    const logout = async () => {
        localStorage.removeItem("jt");
        const response = await fetch('/users/logout?username='+localStorage.getItem("user"), {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        localStorage.removeItem("user")
    }

    return (
        <div className="content">
            <div className="heading-bar">
                <div style={{ paddingTop: '10px' }}>Dashboard <Link to="/login" onClick={e => logout()}><IoIosLogOut id="logout-btn" /></Link></div>
            </div>
            <div className="row">
                <div className="col-sm-8">
                    <div className="l-graph-container">
                        <div className="title">Day</div>
                        <div className="sub-title">SALE & COST</div>
                        {day}
                        <div className="footers">
                        <div className="desc">{footerDate}</div>
                            <div className="nav-btns"><BiLeftArrow className="nav-btn" onClick={e => previous("day")} style={{ fontSize: '25px', marginRight: '5px' }} /><BiRightArrow className="nav-btn" onClick={e => next("day")} style={{ fontSize: '25px' }} /></div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="p-chart-container">
                        <div style={{ marginTop: '10px', textAlign: 'center', fontSize: '22px' }}>Past One Month Sales</div>
                        {salesDChart}
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-8">
                    <div className="l-graph-container">
                        <div className="title">Month</div>
                        <div className="sub-title">SALE & COST</div>
                        {month}
                        <div className="footers">
                        <div className="desc">{footerMonth}</div>
                            <div className="nav-btns"><BiLeftArrow className="nav-btn" onClick={e => previous("month")} style={{ fontSize: '25px', marginRight: '5px' }} /><BiRightArrow className="nav-btn" onClick={e => next("month")} style={{ fontSize: '25px' }} /></div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="p-chart-container">
                        <div className="top-detail-heading">Top 5 Items of Past One Month</div>
                        <table className="top-detail-table">
                            <thead><th style={{ textAlign: 'left', paddingLeft: '50px' }}>Item</th><th style={{ textAlign: 'right', paddingRight: '50px' }}>Quantity</th></thead>
                            <tbody>{topItems}</tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-8">
                    <div className="l-graph-container">
                        <div className="title">Year</div>
                        <div className="sub-title">SALE & COST</div>
                        {year}
                        <div className="footers">
                        <div className="desc">{footerYear}</div>
                            <div className="nav-btns"><BiLeftArrow className="nav-btn" onClick={e => previous("year")} style={{ fontSize: '25px', marginRight: '5px' }} /><BiRightArrow className="nav-btn" onClick={e => next("year")} style={{ fontSize: '25px' }} /></div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                <div className="p-chart-container">
                        <div className="top-detail-heading">Top 5 Categories of Past One Month</div>
                        <table className="top-detail-table">
                            <thead><th style={{ textAlign: 'left', paddingLeft: '50px' }}>Category</th><th style={{ textAlign: 'right', paddingRight: '50px' }}>Quantity</th></thead>
                            <tbody>{topCategories}</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default MainDashboard;
import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import DatePicker from 'react-datepicker';
import download from 'downloadjs';
import Modal from 'react-modal';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { IoIosLogOut } from 'react-icons/io';
import { useHistory, Link } from 'react-router-dom';
import CryptoJS from 'crypto-js';

import { SalesItemTable } from './reportTables';

import 'react-datepicker/dist/react-datepicker.css';
import '../../style/report.css';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > * + *': {
        marginLeft: theme.spacing(5),
        }
    },
}));

function SalesItem() {
    const [anchorEl, setAnchorEl] = useState(null);
    const [categoryCheck, setCategoryCheck] = useState(true);
    const [subCategoryCheck, setSubCategoryCheck] = useState(true);
    const [supplierCheck, setSupplierCheck] = useState(true);
    const [category, setCategory] = useState("");
    const [categoryOpts, setCategoryOpts] = useState();
    const [subCategory, setSubCategory] = useState("");
    const [subCategoryOpts, setSubCategoryOpts] = useState();
    const [supplier, setSupplier] = useState("");
    const [supplierOpts, setSupplierOpts] = useState();
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [showModal, setShowModal] = useState(false);
    const [modalStyle, setModalStyle] = useState({overlay: {zIndex: 2}});
    const [modalContent, setModalContent] = useState("");
    const [salesItemTblRows, setSalesItemTblRows] = useState();
    const classes = useStyles();
    var history = useHistory();

    useEffect(() => {
        if (categoryCheck) {
            document.getElementsByClassName("filters")[0].style.display = "inline-block";
        } else {
            document.getElementsByClassName("filters")[0].style.display = "none";
            setCategory("");
            getData(startDate, endDate, category, subCategory, supplier);
        }
    }, [categoryCheck]);

    useEffect(() => {
        if (subCategoryCheck) {
            document.getElementsByClassName("filters")[1].style.display = "inline-block";
        } else {
            document.getElementsByClassName("filters")[1].style.display = "none";
            setSubCategory("");
            getData(startDate, endDate, category, subCategory, supplier);
        }
    }, [subCategoryCheck]);

    useEffect(() => {
        if (supplierCheck) {
            document.getElementsByClassName("filters")[2].style.display = "inline-block";
        } else {
            document.getElementsByClassName("filters")[2].style.display = "none";
            setSupplier("");
            getData(startDate, endDate, category, subCategory, supplier);
        }
    }, [supplierCheck]);
    
    useEffect(() => {
        if (localStorage.getItem("jt") === null) {
            history.push("/login")
            return;
        }

        window.addEventListener('scroll', e => {
            if (window.scrollY > 10) {
                document.getElementsByClassName("heading-bar")[0].style.boxShadow = "0px 0px 5px 0px rgb(196, 196, 196)";
            } else {
                document.getElementsByClassName("heading-bar")[0].style.boxShadow = "0px 0px 0px 0px rgb(196, 196, 196)";
            }
        });

        getCategories();
        getSubCategories();
        getSuppliers();
        getData(startDate, endDate, category, subCategory, supplier);
    }, [])

    useEffect(() => {
        getSubCategories();
    }, [category])


    useEffect(() => {
        getData(startDate, endDate, category, subCategory, supplier);
    }, [startDate, endDate, category, subCategory, supplier]);

    const getCategories = async () => {
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        const response = await fetch('/sales-item/category', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();
        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }

        if (responseObj[0].length > 0) {
            const categoryListObject = responseObj[0].map((element, key) => (
                <MenuItem selected={category === element.category_name} value={element.category_name}>{element.category_name}</MenuItem>
            ));
            setCategoryOpts(categoryListObject);
        } else if (responseObj[0].length != 0) {
            alert(JSON.stringify(responseObj));
        }
    }

    const getSubCategories = async () => {
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        const response = await fetch('/sales-item/sub-category?category='+category, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();
        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }

        if (responseObj[0].length > 0) {
            const subCategoryListObject = responseObj[0].map((element, key) => (
                <MenuItem selected={category === element.sub_category} value={element.sub_category}>{element.sub_category}</MenuItem>
            ));
            setSubCategoryOpts(subCategoryListObject);
            setSubCategory("");
        } else if (responseObj[0].length != 0) {
            alert(JSON.stringify(responseObj));
        }
    }

    const getSuppliers = async () => {
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        const response = await fetch('/sales-item/supplier', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();
        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }

        if (responseObj[0].length > 0) {
            const supplierListObject = responseObj[0].map((element, key) => (
                <MenuItem selected={supplier === element.name} value={element.id}>{element.name}</MenuItem>
            ));
            setSupplierOpts(supplierListObject);
        } else if (responseObj[0].length != 0) {
            alert(JSON.stringify(responseObj));
        }
    }

    const getData = async (startDate, endDate, category, subCategory, supplier) => {
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);
        
        let from = startDate.toISOString().slice(0, 10);
        let to = endDate.toISOString().slice(0, 10);
        const response = await fetch('/sales-item?from='+from+'&to='+to+'&category='+category+'&subCategory='+subCategory+'&supplier='+supplier, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();
        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        } 

        let salesItemTblRows = [];    
        if (responseObj["sales"].length == 0) {
            setSalesItemTblRows(<tr><td style={{ color: 'green', fontWeight: 'bold', textAlign: 'left' }}>No Records</td><td style={{ textAlign: 'left' }}>-</td><td style={{ textAlign: 'left' }}>-</td><td style={{ textAlign: 'center' }}>-</td></tr>);
        } else if (responseObj["sales"].length > 0) {
            for (let i=0; i<responseObj["sales"].length; i++) {
                salesItemTblRows.push(<tr><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["item_name"]}</td><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["category"]}</td><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["sub_category"]}</td><td style={{ textAlign: 'center' }}>{parseFloat(responseObj["sales"][i]["qty"].toFixed(3))}</td></tr>);
            } 
            setSalesItemTblRows(salesItemTblRows);
        } else {
            alert(JSON.stringify(responseObj));
        }

        
    }

    const openModal = () => {
        setShowModal(true);
    }

    const closeModal = () => {
        setShowModal(false);
    }

    const chooseReportType = (type) => {
        if (type == "sales-item") {
            setModalContent(
                <>
                    <div className={classes.root} id="loading-icon"><CircularProgress style={{ position: 'absolute', right: '0', height: '25px', width: '25px', margin: '5px' }} /></div>
                    <div style={{ fontSize: '20px', paddingTop: '20px', paddingBottom: '15px', fontWeight: 'bold' }}>Sales Item Report</div>
                    <button type="button" onClick={e => getSalesItemReportExcel()} className="download-sub-btns">Download Excel</button>
                    <button type="button" onClick={e => getSalesItemReportPDF()} className="download-sub-btns">Download PDF</button>
                </>
            );
        }

        openModal();
    }

    const getSalesItemReportExcel = async () => {
        const username = localStorage.getItem('user');
        document.getElementById("loading-icon").style.display = "block";
        let from = startDate.toISOString().slice(0, 10);
        let to = endDate.toISOString().slice(0, 10);
        const response = await fetch('/sales-item/download/report/excel?from='+from+'&to='+to+'&category='+category+'&subCategory='+subCategory+'&username='+username+'&supplier='+supplier, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        const responseObj = await response.blob();
        document.getElementById("loading-icon").style.display = "none";
        download(responseObj, "Sales Items Report on "+new Date().toISOString().slice(0, 10)+".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    }

    const getSalesItemReportPDF = async () => {
        const username = localStorage.getItem('user');
        document.getElementById("loading-icon").style.display = "block";
        let from = startDate.toISOString().slice(0, 10);
        let to = endDate.toISOString().slice(0, 10);
        const response = await fetch('/sales-item/download/report/pdf?from='+from+'&to='+to+'&category='+category+'&subCategory='+subCategory+'&username='+username+'&supplier='+supplier, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        const responseObj = await response.blob();
        const fileURL = URL.createObjectURL(responseObj);//Open the URL on new Window
        document.getElementById("loading-icon").style.display = "none";
        // download(fileURL);
        // window.open(fileURL);
        
        // create <a> tag dinamically
        var fileLink = document.createElement('a');
        fileLink.href = fileURL;

        // it forces the name of the downloaded file
        fileLink.download = "Sales Items Report on "+new Date().toISOString().slice(0, 10);

        // triggers the click event
        fileLink.click();
    }

    const logout = async () => {
        localStorage.removeItem("jt");
        const response = await fetch('/users/logout?username='+localStorage.getItem("user"), {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        localStorage.removeItem("user")
    }

    return (
        <div className="content">
            <div className="heading-bar"><div style={{ paddingTop: '10px' }}>Sales Items <Link to="/login" onClick={e => logout()}><IoIosLogOut id="logout-btn" /></Link></div></div>
            <div>
                <Button id="customize-btn" aria-controls="filter-items" aria-haspopup="true" onClick={e => setAnchorEl(e.currentTarget)} variant="contained" color="primary" style={{ marginLeft: '2%', marginTop: '1%' }}>
                    Customize Filters
                </Button>
                <Menu
                    id="filter-items"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={e => setAnchorEl(null)}
                >
                    <MenuItem>Category<Checkbox checked={categoryCheck} onChange={e => { if (categoryCheck === false) { setCategoryCheck(true); } else { setCategoryCheck(false) } }} /></MenuItem>
                    <MenuItem>Sub Category<Checkbox checked={subCategoryCheck} onChange={e => { if (subCategoryCheck === false) { setSubCategoryCheck(true); } else { setSubCategoryCheck(false) } }} /></MenuItem>
                    <MenuItem>Supplier<Checkbox checked={supplierCheck} onChange={e => { if (supplierCheck === false) { setSupplierCheck(true); } else { setSupplierCheck(false) } }} /></MenuItem>
                </Menu>
            </div>
            <div id="dashboard-content">
                <div id="filter-content">
                    <div className="filter-sub-content">
                        <FormControl className="filters">
                            <InputLabel id="category-label" className="labels">Category</InputLabel>
                            <Select labelId="category-label" value={category} onChange={e => setCategory(e.target.value)} className="select">
                                <MenuItem selected={category === ""} value="">ALL</MenuItem>
                                {categoryOpts}
                            </Select>
                        </FormControl>
                        <FormControl className="filters">
                            <InputLabel id="sub-category-label" className="labels">Sub Category</InputLabel>
                            <Select labelId="sub-category-label" value={subCategory} onChange={e => setSubCategory(e.target.value)} className="select">
                                <MenuItem selected={subCategory === ""} value="">ALL</MenuItem>
                                {subCategoryOpts}
                            </Select>
                        </FormControl>
                        <FormControl className="filters">
                            <InputLabel id="supplier-label" className="labels">Supplier</InputLabel>
                            <Select labelId="supplier-label" value={supplier} onChange={e => setSupplier(e.target.value)} className="select">
                                <MenuItem selected={supplier === ""} value="">ALL</MenuItem>
                                {supplierOpts}
                                </Select>
                        </FormControl>
                    </div>
                    <table id="date-range">
                        <tr>
                            <td>From</td>
                            <td>
                                <DatePicker
                                    selected={startDate}
                                    onChange={date => setStartDate(date)}
                                    selectsStart
                                    startDate={startDate}
                                    endDate={endDate}
                                    className="date-elements"
                                />
                            </td>
                            <td>To</td>
                            <td>
                                <DatePicker
                                    selected={endDate}
                                    onChange={date => setEndDate(date)}
                                    selectsEnd
                                    startDate={startDate}
                                    endDate={endDate}
                                    minDate={startDate}
                                    className="date-elements"
                                />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <div id="sales">
                <div id="download-btns">
                    <button type="button" onClick={e => chooseReportType("sales-item")} className="download-btns">Download Report</button>
                </div>
                <div id="tbl-container"><SalesItemTable rows={salesItemTblRows} /></div>
            </div>
            <Modal style={ modalStyle } id="download-report-type-modal" isOpen={showModal} closeOnDocumentClick onRequestClose={e => closeModal()}>
                {modalContent}
            </Modal>
        </div>
    );
}

export default SalesItem;
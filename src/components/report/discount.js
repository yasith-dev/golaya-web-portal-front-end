import React, { useState, useEffect } from 'react';
import download from 'downloadjs';
import Modal from 'react-modal';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { IoIosLogOut } from 'react-icons/io';
import { useHistory, Link } from 'react-router-dom';
import CryptoJS from 'crypto-js';
import DatePicker from 'react-datepicker';

import { DiscountTable } from './reportTables';

import 'react-datepicker/dist/react-datepicker.css';
import '../../style/report.css';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > * + *': {
        marginLeft: theme.spacing(5),
        }
    },
}));

function Discount() {
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [showModal, setShowModal] = useState(false);
    const [modalStyle, setModalStyle] = useState({overlay: {zIndex: 2}});
    const [modalContent, setModalContent] = useState("");
    const [discountTblRows, setDiscountTblRows] = useState();
    const classes = useStyles();
    var history = useHistory();

    useEffect(() => {
        if (localStorage.getItem("jt") === null) {
            history.push("/login")
            return;
        }

        window.addEventListener('scroll', e => {
            if (window.scrollY > 10) {
                document.getElementsByClassName("heading-bar")[0].style.boxShadow = "0px 0px 5px 0px rgb(196, 196, 196)";
            } else {
                document.getElementsByClassName("heading-bar")[0].style.boxShadow = "0px 0px 0px 0px rgb(196, 196, 196)";
            }
        });
        
        getData(startDate, endDate);
    }, [])

    useEffect(() => {
        getData(startDate, endDate);
    }, [startDate, endDate])

    const getData = async (startDate, endDate) => {
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        let from = startDate.toISOString().slice(0, 10);
        let to = endDate.toISOString().slice(0, 10);
        const response = await fetch('discounts?from='+from+'&to='+to, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();
        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }

        let discountTblRows = [], discount_percentage = 0, discount = 0;
        if (responseObj["rows"].length == 0) {
            setDiscountTblRows(<tr><td style={{ color: 'green', fontWeight: 'bold', textAlign: 'left' }}>No Records</td><td style={{ textAlign: 'right' }}>-</td><td style={{ textAlign: 'right' }}>-</td><td style={{ textAlign: 'center' }}>-</td></tr>);
        } else if (responseObj["rows"].length > 0) {
            for (let i=0; i<responseObj["rows"].length; i++) {
                if(responseObj["rows"][i]["discount_is_percentage"] === 1){
                    discount_percentage = responseObj["rows"][i]["discount_amount"];
                    discount = ((responseObj["rows"][i]["sub_total"] - responseObj["rows"][i]["items_discount_total"]) * discount_percentage / 100).toFixed(2);
                }else{
                    discount = responseObj["rows"][i]["discount_amount"].toFixed(2);
                }
                discountTblRows.push(<tr><td style={{ textAlign: 'left' }}>{responseObj["rows"][i]["invoice_no"]}</td><td style={{ textAlign: 'right' }}>{discount_percentage}</td><td style={{ textAlign: 'right' }}>{discount}</td><td style={{ textAlign: 'center' }}>{responseObj["rows"][i]["discount_username"]}</td></tr>);
            } 
            setDiscountTblRows(discountTblRows);
        } else {
            alert(JSON.stringify(responseObj));
        }
        
    }

    const openModal = () => {
        setShowModal(true);
    }

    const closeModal = () => {
        setShowModal(false);
    }

    const chooseReportType = (type) => {
        if (type == "discount") {
            setModalContent(
                <>
                    <div className={classes.root} id="loading-icon"><CircularProgress style={{ position: 'absolute', right: '0', height: '25px', width: '25px', margin: '5px' }} /></div>
                    <div style={{ fontSize: '20px', paddingTop: '20px', paddingBottom: '15px', fontWeight: 'bold' }}>Discounts by Invoice Report</div>
                    <button type="button" onClick={e => getDiscountReportExcel()} className="download-sub-btns">Download Excel</button>
                    <button type="button" onClick={e => getDiscountReportPDF()} className="download-sub-btns">Download PDF</button>
                </>
            );
        }

        openModal();
    }

    const getDiscountReportExcel = async () => {
        const username = localStorage.getItem('user');
        document.getElementById("loading-icon").style.display = "block";
        let from = startDate.toISOString().slice(0, 10);
        let to = endDate.toISOString().slice(0, 10);
        const response = await fetch('/discounts/download/invoice-discount-report/excel?from='+from+'&to='+to+'&username='+username, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        const responseObj = await response.blob();
        document.getElementById("loading-icon").style.display = "none";
        download(responseObj, "Discounts by Invoice Report on "+new Date().toISOString().slice(0, 10)+".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    }

    const getDiscountReportPDF = async () => {
        const username = localStorage.getItem('user');
        document.getElementById("loading-icon").style.display = "block";
        let from = startDate.toISOString().slice(0, 10);
        let to = endDate.toISOString().slice(0, 10);
        const response = await fetch('/discounts/download/invoice-discount-report/pdf?from='+from+'&to='+to+'&username='+username, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        const responseObj = await response.blob();
        const fileURL = URL.createObjectURL(responseObj);//Open the URL on new Window
        document.getElementById("loading-icon").style.display = "none";
        // download(fileURL);
        // window.open(fileURL);

        // create <a> tag dinamically
        var fileLink = document.createElement('a');
        fileLink.href = fileURL;

        // it forces the name of the downloaded file
        fileLink.download = "Discounts by Invoice Report on "+new Date().toISOString().slice(0, 10);

        // triggers the click event
        fileLink.click();
    }

    const logout = async () => {
        localStorage.removeItem("jt");
        const response = await fetch('/users/logout?username='+localStorage.getItem("user"), {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        localStorage.removeItem("user")
    }

    return (
        <div className="content">
            <div className="heading-bar">
                <div style={{ paddingTop: '10px' }}>Discount <Link to="/login" onClick={e => logout()}><IoIosLogOut id="logout-btn" /></Link></div>
            </div>
            <div style={{ height: '20px' }}></div>
            <div id="dashboard-content">
                <div style={{ height: '5px' }}></div>
                <div id="filter-content">
                    <table id="date-range">
                        <tr>
                            <td>From</td>
                            <td>
                                <DatePicker
                                    selected={startDate}
                                    onChange={date => setStartDate(date)}
                                    selectsStart
                                    startDate={startDate}
                                    endDate={endDate}
                                    className="date-elements"
                                />
                            </td>
                            <td>To</td>
                            <td>
                                <DatePicker
                                    selected={endDate}
                                    onChange={date => setEndDate(date)}
                                    selectsEnd
                                    startDate={startDate}
                                    endDate={endDate}
                                    minDate={startDate}
                                    className="date-elements"
                                />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>    
            <div id="sales">
                <div id="download-btns">
                    <button type="button" onClick={e => chooseReportType("discount")} className="download-btns">Download Report</button>
                </div>
                <div id="tbl-container"><DiscountTable rows={discountTblRows} /></div>
            </div>
            <Modal style={ modalStyle } id="download-report-type-modal" isOpen={showModal} closeOnDocumentClick onRequestClose={e => closeModal()}>
                {modalContent}
            </Modal>
        </div>
    );
}

export default Discount;
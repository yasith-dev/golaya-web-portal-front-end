import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import DatePicker from 'react-datepicker';
import { Line } from 'react-chartjs-2';
import 'chartjs-plugin-labels';
import download from 'downloadjs';
import Modal from 'react-modal';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { IoIosLogOut } from 'react-icons/io';
import { useHistory, Link } from 'react-router-dom';
import CryptoJS from 'crypto-js';

import { StockTable } from './reportTables';

import 'react-datepicker/dist/react-datepicker.css';
import '../../style/report.css';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > * + *': {
        marginLeft: theme.spacing(5),
        }
    },
}));

function Stock() {
    const [anchorEl, setAnchorEl] = useState(null);
    const [categoryCheck, setCategoryCheck] = useState(true);
    const [subCategoryCheck, setSubCategoryCheck] = useState(true);
    const [supplierCheck, setSupplierCheck] = useState(true);
    const [expireCheck, setExpireCheck] = useState(true);
    const [category, setCategory] = useState("");
    const [categoryOpts, setCategoryOpts] = useState();
    const [subCategory, setSubCategory] = useState("");
    const [subCategoryOpts, setSubCategoryOpts] = useState();
    const [supplier, setSupplier] = useState("");
    const [supplierOpts, setSupplierOpts] = useState();
    const [expire, setExpire] = useState("");
    const [showModal, setShowModal] = useState(false);
    const [modalStyle, setModalStyle] = useState({overlay: {zIndex: 2}});
    const [modalContent, setModalContent] = useState("");
    const [stockTblRows, setStockTblRows] = useState();
    const classes = useStyles();
    var history = useHistory();

    useEffect(() => {
        if (categoryCheck) {
            document.getElementsByClassName("filters")[0].style.display = "inline-block";
        } else {
            document.getElementsByClassName("filters")[0].style.display = "none";
            setCategory("");
            getData(category, subCategory, supplier, expire);
        }
    }, [categoryCheck]);

    useEffect(() => {
        if (subCategoryCheck) {
            document.getElementsByClassName("filters")[1].style.display = "inline-block";
        } else {
            document.getElementsByClassName("filters")[1].style.display = "none";
            setSubCategory("");
            getData(category, subCategory, supplier, expire);
        }
    }, [subCategoryCheck]);

    useEffect(() => {
        if (supplierCheck) {
            document.getElementsByClassName("filters")[2].style.display = "inline-block";
        } else {
            document.getElementsByClassName("filters")[2].style.display = "none";
            setSupplier("");
            getData(category, subCategory, supplier, expire);
        }
    }, [supplierCheck]);

    useEffect(() => {
        if (expireCheck) {
            document.getElementsByClassName("filters")[3].style.display = "inline-block";
        } else {
            document.getElementsByClassName("filters")[3].style.display = "none";
            setExpire("");
            getData(category, subCategory, supplier, expire);
        }
    }, [expireCheck]);
    
    useEffect(() => {
        if (localStorage.getItem("jt") === null) {
            history.push("/login")
            return;
        }

        window.addEventListener('scroll', e => {
            if (window.scrollY > 10) {
                document.getElementsByClassName("heading-bar")[0].style.boxShadow = "0px 0px 5px 0px rgb(196, 196, 196)";
            } else {
                document.getElementsByClassName("heading-bar")[0].style.boxShadow = "0px 0px 0px 0px rgb(196, 196, 196)";
            }
        });
        
        getCategories();
        getSubCategories();
        getSuppliers();
        getData(category, subCategory, supplier, expire);
    }, [])

    useEffect(() => {
        getSubCategories();
    }, [category])


    useEffect(() => {
        getData(category, subCategory, supplier, expire);
    }, [category, subCategory, supplier, expire]);

    const getCategories = async () => {
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        const response = await fetch('/sales-item/category', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();
        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }

        if (responseObj[0].length > 0) {
            const categoryListObject = responseObj[0].map((element, key) => (
                <MenuItem selected={category === element.category_name} value={element.category_name}>{element.category_name}</MenuItem>
            ));
            setCategoryOpts(categoryListObject);
        } else if (responseObj[0].length != 0) {
            alert(JSON.stringify(responseObj));
        }
    }

    const getSubCategories = async () => {
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        const response = await fetch('/sales-item/sub-category?category='+category, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();
        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }

        if (responseObj[0].length > 0) {
            const subCategoryListObject = responseObj[0].map((element, key) => (
                <MenuItem selected={category === element.sub_category} value={element.sub_category}>{element.sub_category}</MenuItem>
            ));
            setSubCategoryOpts(subCategoryListObject);
            setSubCategory("");
        } else if (responseObj[0].length != 0) {
            alert(JSON.stringify(responseObj));
        }
    }

    const getSuppliers = async () => {
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        const response = await fetch('/sales-item/supplier', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json();
        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }

        if (responseObj[0].length > 0) {
            const supplierListObject = responseObj[0].map((element, key) => (
                <MenuItem selected={supplier === element.name} value={element.id}>{element.name}</MenuItem>
            ));
            setSupplierOpts(supplierListObject);
        } else if (responseObj[0].length != 0) {
            alert(JSON.stringify(responseObj));
        }
    }

    const getData = async (category, subCategory, supplier, expire) => {
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        const response = await fetch('/stocks/all?category='+category+'&subCategory='+subCategory+'&supplier='+supplier+'&expire='+expire, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json(); 
        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        }

        let stockTblRows = [];    
        if (responseObj["sales"].length == 0) {
            setStockTblRows(<tr><td style={{ color: 'green', fontWeight: 'bold', textAlign: 'left' }}>No Records</td><td style={{ textAlign: 'left' }}>-</td><td style={{ textAlign: 'left' }}>-</td><td style={{ textAlign: 'left' }}>-</td><td style={{ textAlign: 'right' }}>-</td><td style={{ textAlign: 'right' }}>-</td><td style={{ textAlign: 'center' }}>-</td><td style={{ textAlign: 'center' }}>-</td><td style={{ textAlign: 'center' }}>-</td></tr>);
        } else if (responseObj["sales"].length > 0) {
            for (let i=0; i<responseObj["sales"].length; i++) {
                if (responseObj["sales"][i]["qty"] < responseObj["sales"][i]["reorder_qty"]) {
                    stockTblRows.push(<tr style={{ backgroundColor: '#ffa3a3' }}><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["item_name"]}</td><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["item_brand"]}</td><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["category"]}</td><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["sub_category"]}</td><td style={{ textAlign: 'right' }}>{responseObj["sales"][i]["cost_price"].toFixed(2)}</td><td style={{ textAlign: 'right' }}>{responseObj["sales"][i]["selling_price"].toFixed(2)}</td><td style={{ textAlign: 'center' }}>{parseFloat(responseObj["sales"][i]["qty"].toFixed(3))}</td><td style={{ textAlign: 'center' }}>{parseFloat(responseObj["sales"][i]["alert_qty"].toFixed(3))}</td><td style={{ textAlign: 'center' }}>{parseFloat(responseObj["sales"][i]["re_order_qty"].toFixed(3))}</td></tr>);
                } else if (responseObj["sales"][i]["qty"] < responseObj["sales"][i]["alert_qty"]) {
                    stockTblRows.push(<tr style={{ backgroundColor: '#fff9a3' }}><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["item_name"]}</td><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["item_brand"]}</td><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["category"]}</td><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["sub_category"]}</td><td style={{ textAlign: 'right' }}>{responseObj["sales"][i]["cost_price"].toFixed(2)}</td><td style={{ textAlign: 'right' }}>{responseObj["sales"][i]["selling_price"].toFixed(2)}</td><td style={{ textAlign: 'center' }}>{parseFloat(responseObj["sales"][i]["qty"].toFixed(3))}</td><td style={{ textAlign: 'center' }}>{parseFloat(responseObj["sales"][i]["alert_qty"].toFixed(3))}</td><td style={{ textAlign: 'center' }}>{parseFloat(responseObj["sales"][i]["re_order_qty"].toFixed(3))}</td></tr>);
                } else {
                    stockTblRows.push(<tr><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["item_name"]}</td><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["item_brand"]}</td><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["category"]}</td><td style={{ textAlign: 'left' }}>{responseObj["sales"][i]["sub_category"]}</td><td style={{ textAlign: 'right' }}>{responseObj["sales"][i]["cost_price"].toFixed(2)}</td><td style={{ textAlign: 'right' }}>{responseObj["sales"][i]["selling_price"].toFixed(2)}</td><td style={{ textAlign: 'center' }}>{parseFloat(responseObj["sales"][i]["qty"].toFixed(3))}</td><td style={{ textAlign: 'center' }}>{parseFloat(responseObj["sales"][i]["alert_qty"].toFixed(3))}</td><td style={{ textAlign: 'center' }}>{parseFloat(responseObj["sales"][i]["re_order_qty"].toFixed(3))}</td></tr>);
                } 
            } 
            setStockTblRows(stockTblRows);
        } else {
            alert(JSON.stringify(responseObj));
        }
        
    }

    const openModal = () => {
        setShowModal(true);
    }

    const closeModal = () => {
        setShowModal(false);
    }

    const chooseReportType = (type) => {
        if (type == "stock") {
            setModalContent(
                <>
                    <div className={classes.root} id="loading-icon"><CircularProgress style={{ position: 'absolute', right: '0', height: '25px', width: '25px', margin: '5px' }} /></div>
                    <div style={{ fontSize: '20px', paddingTop: '20px', paddingBottom: '15px', fontWeight: 'bold' }}>Stock Report</div>
                    <button type="button" onClick={e => getStockReportExcel()} className="download-sub-btns">Download Excel</button>
                    <button type="button" onClick={e => getStockReportPDF()} className="download-sub-btns">Download PDF</button>
                </>
            );
        }

        openModal();
    }

    const getStockReportExcel = async () => {
        const username = localStorage.getItem('user');
        document.getElementById("loading-icon").style.display = "block";
        const response = await fetch('/stocks/download/report/excel?username='+username+'&category='+category+'&subCategory='+subCategory+'&supplier='+supplier+'&expire='+expire, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        const responseObj = await response.blob();
        document.getElementById("loading-icon").style.display = "none";
        download(responseObj, "Stock Report on "+new Date().toISOString().slice(0, 10)+".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    }

    const getStockReportPDF = async () => {
        const username = localStorage.getItem('user');
        document.getElementById("loading-icon").style.display = "block";
        const response = await fetch('/stocks/download/report/pdf?username='+username+'&category='+category+'&subCategory='+subCategory+'&supplier='+supplier+'&expire='+expire, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        const responseObj = await response.blob();
        const fileURL = URL.createObjectURL(responseObj);//Open the URL on new Window
        document.getElementById("loading-icon").style.display = "none";
        // download(fileURL);
        // window.open(fileURL);

        // create <a> tag dinamically
        var fileLink = document.createElement('a');
        fileLink.href = fileURL;

        // it forces the name of the downloaded file
        fileLink.download = "Stock Report on "+new Date().toISOString().slice(0, 10);

        // triggers the click event
        fileLink.click();
    }

    const logout = async () => {
        localStorage.removeItem("jt");
        const response = await fetch('/users/logout?username='+localStorage.getItem("user"), {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        localStorage.removeItem("user")
    }

    return (
        <div className="content">
            <div className="heading-bar"><div style={{ paddingTop: '10px' }}>Stock <Link to="/login" onClick={e => logout()}><IoIosLogOut id="logout-btn" /></Link></div></div> 
            <div>
                <Button id="customize-btn" aria-controls="filter-items" aria-haspopup="true" onClick={e => setAnchorEl(e.currentTarget)} variant="contained" color="primary" style={{ marginLeft: '2%', marginTop: '1%' }}>
                    Customize Filters
                </Button>
                <Menu
                    id="filter-items"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={e => setAnchorEl(null)}
                >
                    <MenuItem>Category<Checkbox checked={categoryCheck} onChange={e => { if (categoryCheck === false) { setCategoryCheck(true); } else { setCategoryCheck(false) } }} /></MenuItem>
                    <MenuItem>Sub Category<Checkbox checked={subCategoryCheck} onChange={e => { if (subCategoryCheck === false) { setSubCategoryCheck(true); } else { setSubCategoryCheck(false) } }} /></MenuItem>
                    <MenuItem>Supplier<Checkbox checked={supplierCheck} onChange={e => { if (supplierCheck === false) { setSupplierCheck(true); } else { setSupplierCheck(false) } }} /></MenuItem>
                    <MenuItem>Expiring Items<Checkbox checked={expireCheck} onChange={e => { if (expireCheck === false) { setExpireCheck(true); } else { setExpireCheck(false) } }} /></MenuItem>
                </Menu>
            </div>
            <div id="dashboard-content">
                <div id="filter-content">
                    <div className="filter-sub-content">
                        <FormControl className="filters">
                            <InputLabel id="category-label" className="labels">Category</InputLabel>
                            <Select labelId="category-label" value={category} onChange={e => setCategory(e.target.value)} className="select">
                                <MenuItem selected={category === ""} value="">ALL</MenuItem>
                                {categoryOpts}
                            </Select>
                        </FormControl>
                        <FormControl className="filters">
                            <InputLabel id="sub-category-label" className="labels">Sub Category</InputLabel>
                            <Select labelId="sub-category-label" value={subCategory} onChange={e => setSubCategory(e.target.value)} className="select">
                                <MenuItem selected={subCategory === ""} value="">ALL</MenuItem>
                                {subCategoryOpts}
                            </Select>
                        </FormControl>
                        <FormControl className="filters">
                            <InputLabel id="supplier-label" className="labels">Supplier</InputLabel>
                            <Select labelId="supplier-label" value={supplier} onChange={e => setSupplier(e.target.value)} className="select">
                                <MenuItem selected={supplier === ""} value="">ALL</MenuItem>
                                {supplierOpts}
                                </Select>
                        </FormControl>
                        <FormControl className="filters">
                            <InputLabel id="expire-label" className="labels">Expiring Items</InputLabel>
                            <Select labelId="expire-label" value={expire} onChange={e => setExpire(e.target.value)} className="select">
                                <MenuItem selected={expire === ""} value="">Don't show</MenuItem>
                                <MenuItem selected={expire === 1} value={1}>Show</MenuItem>
                                </Select>
                        </FormControl>
                    </div>
                </div>
            </div>    
            <div id="sales">
                <div id="download-btns">
                    <button type="button" onClick={e => chooseReportType("stock")} className="download-btns">Download Report</button>
                </div>
                <div id="tbl-container"><StockTable rows={stockTblRows} /></div>
            </div>
            <Modal style={ modalStyle } id="download-report-type-modal" isOpen={showModal} closeOnDocumentClick onRequestClose={e => closeModal()}>
                {modalContent}
            </Modal>
        </div>
    );
}

export default Stock;
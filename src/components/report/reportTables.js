import React, { useState, useEffect } from 'react';

export function SalesTable(props) {
    return (
        <div id="tbl-wrapper">
            <table id="tbl">
                <thead><th>Invoice No</th><th style={{ textAlign: 'right' }}>Sub Total (Rs)</th><th style={{ textAlign: 'right' }}>Discount (Rs)</th><th style={{ textAlign: 'right' }}>Grand Total (Rs)</th><th style={{ textAlign: 'right' }}>Cost Total (Rs)</th><th style={{ textAlign: 'center' }}>POS No.</th><th style={{ textAlign: 'center' }}>Timestamp</th></thead>
                <tbody>{props.rows}</tbody>
            </table>
        </div>
    );
}

export function SalesItemTable(props) {
    return (
        <div id="tbl-wrapper">
            <table id="tbl">
                <thead><th style={{ textAlign: 'left' }}>Item</th><th style={{ textAlign: 'left' }}>Category</th><th style={{ textAlign: 'left' }}>Sub Category</th><th style={{ textAlign: 'center' }}>Sold Quantity</th></thead>
                <tbody>{props.rows}</tbody>
            </table>
        </div>
    );
}

export function PurchasingTable(props) {
    return (
        <div id="tbl-wrapper">
            <table id="tbl">
                <thead><th style={{ textAlign: 'left' }}>Item Name</th><th style={{ textAlign: 'left' }}>Brand</th><th style={{ textAlign: 'left' }}>Category</th><th style={{ textAlign: 'left' }}>Sub Category</th><th style={{ textAlign: 'right' }}>Cost Price</th><th style={{ textAlign: 'center' }}>Quantity</th><th style={{ textAlign: 'right' }}>Total</th><th style={{ textAlign: 'center' }}>Timestamp</th></thead>
                <tbody>{props.rows}</tbody>
            </table>
        </div>
    );
}

export function StockTable(props) {
    return (
        <div id="tbl-wrapper">
            <table id="tbl">
                <thead><th style={{ textAlign: 'left' }}>Item Name</th><th style={{ textAlign: 'left' }}>Brand</th><th style={{ textAlign: 'left' }}>Category</th><th style={{ textAlign: 'left' }}>Sub Category</th><th style={{ textAlign: 'right' }}>Cost Price</th><th style={{ textAlign: 'right' }}>Selling Price</th><th style={{ textAlign: 'center' }}>Quantity</th><th style={{ textAlign: 'center' }}>Alert Quantity</th><th style={{ textAlign: 'center' }}>Reorder Quantity</th></thead>
                <tbody>{props.rows}</tbody>
            </table>
        </div>
    );
}

export function DiscountTable(props) {
    return (
        <div id="tbl-wrapper">
            <table id="tbl">
                <thead><th style={{ textAlign: 'left' }}>Invoice No</th><th style={{ textAlign: 'right' }}>Discount (%)</th><th style={{ textAlign: 'right' }}>Discount Amount (Rs)</th><th style={{ textAlign: 'center' }}>Issued By</th></thead>
                <tbody>{props.rows}</tbody>
            </table>
        </div>
    );
}

export function SummaryTable(props) {
    return (
        <table id="summary-tbl">
            <thead><tr><th>Summary</th><th></th></tr></thead>
            <tbody>{props.rows}</tbody>
        </table>
    );
}
import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import DatePicker from 'react-datepicker';
import { Line } from 'react-chartjs-2';
import 'chartjs-plugin-labels';
import download from 'downloadjs';
import Modal from 'react-modal';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { IoIosLogOut } from 'react-icons/io';
import { useHistory, Link } from 'react-router-dom';
import CryptoJS from 'crypto-js';

import Dashboard from '../dashboard/dashboard';
import { SalesTable, SummaryTable } from './reportTables';

import 'react-datepicker/dist/react-datepicker.css';
import '../../style/report.css';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > * + *': {
        marginLeft: theme.spacing(5),
        }
    },
}));

function Sales() {
    const [anchorEl, setAnchorEl] = useState(null);
    const [ptCheck, setPtCheck] = useState(true);
    const [paymentType, setPaymentType] = useState("");
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [lineGraph, setLineGraph] = useState();
    const [showModal, setShowModal] = useState(false);
    const [modalStyle, setModalStyle] = useState({overlay: {zIndex: 2}});
    const [modalContent, setModalContent] = useState("");
    const [salesTblRows, setSalesTblRows] = useState();
    const [summaryTblRows, setSummaryTblRows] = useState();
    const classes = useStyles();
    var history = useHistory();

    useEffect(() => {
        if (ptCheck) {
            document.getElementsByClassName("filters")[0].style.display = "inline-block";
        } else {
            document.getElementsByClassName("filters")[0].style.display = "none";
            setPaymentType("");
            getData(startDate, endDate, paymentType);
        }
    }, [ptCheck]);
    
    useEffect(() => {
        if (localStorage.getItem("jt") === null) {
            history.push("/login")
            return;
        }

        window.addEventListener('scroll', e => {
            if (window.scrollY > 10) {
                document.getElementsByClassName("heading-bar")[0].style.boxShadow = "0px 0px 5px 0px rgb(196, 196, 196)";
            } else {
                document.getElementsByClassName("heading-bar")[0].style.boxShadow = "0px 0px 0px 0px rgb(196, 196, 196)";
            }
        });

        getData(startDate, endDate, paymentType);
    }, [])

    useEffect(() => {
        getData(startDate, endDate, paymentType);
    }, [startDate, endDate, paymentType]);

    const getData = async (startDate, endDate, pType) => {
        const ciphertext = localStorage.getItem("jt");
        // Decrypt
        var bytes  = CryptoJS.AES.decrypt(ciphertext, '53KR3TKE');
        var token = bytes.toString(CryptoJS.enc.Utf8);

        let from = startDate.toISOString().slice(0, 10);
        let to = endDate.toISOString().slice(0, 10);
        const response = await fetch('/sales?from='+from+'&to='+to+'&pType='+pType, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const responseObj = await response.json(); 
        if (responseObj["message"] == "Authentication failed") {
            localStorage.removeItem("jt");
            history.push("/login");
            alert("Session has expired! Please login again.");
            return;
        } 

        let salesTblRows = [], orders = 0, grossTotal = 0, netTotal = 0, labelArray = [], dataArray = [];    
        if (responseObj["sales"].length == 0) {
            labelArray = [0];
            dataArray = [0];
            generateLineGraph(labelArray, dataArray);
            setSalesTblRows(<tr><td style={{ color: 'green', fontWeight: 'bold' }}>No Records</td><td style={{ textAlign: 'right' }}>-</td><td style={{ textAlign: 'right' }}>-</td><td style={{ textAlign: 'right' }}>-</td><td style={{ textAlign: 'right' }}>-</td><td style={{ textAlign: 'center' }}>-</td><td style={{ textAlign: 'center' }}>-</td></tr>);
            setSummaryTblRows(
                <>
                <tr><td>No. of Orders</td><td>0</td></tr>
                <tr><td>Gross Total</td><td>0</td></tr>
                <tr><td>Net Total</td><td>0</td></tr>
                </>
            );
        } else if (responseObj["sales"].length > 0) {
            for (let i=0; i<responseObj["salesGraph"].length; i++) {
                labelArray[i] = responseObj["salesGraph"][i]["date"].slice(0, 10);
                dataArray[i] = responseObj["salesGraph"][i]["sum"].toFixed(2);
            }
            generateLineGraph(labelArray, dataArray);
            let discount = 0;
            for (let i=0; i<responseObj["sales"].length; i++) {
                if (typeof responseObj["sales"][i]["discount"] == "undefined") {
                    discount = (0).toFixed(2);
                } else {
                    discount = responseObj["sales"][i]["discount"].toFixed();
                }
                salesTblRows.push(<tr><td>{responseObj["sales"][i]["invoice_no"]}</td><td style={{ textAlign: 'right' }}>{responseObj["sales"][i]["sub_total"].toFixed(2)}</td><td style={{ textAlign: 'right' }}>{discount}</td><td style={{ textAlign: 'right' }}>{responseObj["sales"][i]["grand_total"].toFixed(2)}</td><td style={{ textAlign: 'right' }}>{responseObj["sales"][i]["cost_total"].toFixed(2)}</td><td style={{ textAlign: 'center' }}>{responseObj["sales"][i]["pos_no"]}</td><td style={{ textAlign: 'center' }}>{responseObj["sales"][i]["timestamp"].slice(0, 19).replace('T', ' ')}</td></tr>);
                orders = orders + 1;
                grossTotal = grossTotal + responseObj["sales"][i]["sub_total"];
                netTotal = netTotal + responseObj["sales"][i]["grand_total"];
            } 
            setSalesTblRows(salesTblRows);
            setSummaryTblRows(
                <>
                <tr><td>No. of Orders</td><td style={{ textAlign: 'right' }}>{orders}</td></tr>
                <tr><td>Gross Total</td><td style={{ textAlign: 'right' }}>{grossTotal.toFixed(2)}</td></tr>
                <tr><td>Net Total</td><td style={{ textAlign: 'right' }}>{netTotal.toFixed(2)}</td></tr>
                </>
            );
        } else {
            alert(JSON.stringify(responseObj));
        }

        
    }

    const generateLineGraph = (labelArray, dataArray) => {
        const data = {
            labels: labelArray,
            datasets: [{
                label: 'Filled',
                backgroundColor: '#3293c0',
                borderColor: '#3293c0',
                pointRadius: 0,
                pointHoverRadius: 3,
                borderWidth: 0,
                data: dataArray
            }]
        };

        const options = {
            legend: {
                display: false
            },
            responsive: true,
            /* title: {
                display: true,
                text: 'Chart.js Line Chart'
            }, */
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Date'
                    },
                    ticks: {
                      fontColor: "#9f9f9f",
                      beginAtZero: false,
                      maxTicksLimit: 5,
                      //padding: 20
                    },
                    gridLines: {
                      drawBorder: false,
                      zeroLineColor: "#ccc",
                      color: "rgba(255,255,255,0.05)",
                    },
                  }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Amount'
                    },
                    ticks: {
                      fontColor: "#9f9f9f",
                      beginAtZero: false,
                      maxTicksLimit: 5,
                      //padding: 20
                    },
                    gridLines: {
                      drawBorder: false,
                      zeroLineColor: "#ccc",
                      color: "rgba(255,255,255,0.05)",
                    },
                  }]
            }
        };

        setLineGraph(
            <Line
                data={data}
                options={options}
                width={0}
                id="sales-line-graph"
            />
        );
    }

    const openModal = () => {
        setShowModal(true);
    }

    const closeModal = () => {
        setShowModal(false);
    }

    const chooseReportType = (type) => {
        if (type == "sales") {
            setModalContent(
                <>
                    <div className={classes.root} id="loading-icon"><CircularProgress style={{ position: 'absolute', right: '0', height: '25px', width: '25px', margin: '5px' }} /></div>
                    <div style={{ fontSize: '20px', paddingTop: '20px', paddingBottom: '15px', fontWeight: 'bold' }}>Sales Report</div>
                    <button type="button" onClick={e => getSalesReportExcel()} className="download-sub-btns">Download Excel</button>
                    <button type="button" onClick={e => getSalesReportPDF()} className="download-sub-btns">Download PDF</button>
                </>
            );
        }

        openModal();
    }

    const getSalesReportExcel = async () => {
        const username = localStorage.getItem('user');
        document.getElementById("loading-icon").style.display = "block";
        let from = startDate.toISOString().slice(0, 10);
        let to = endDate.toISOString().slice(0, 10);
        const response = await fetch('/sales/download/report/excel?from='+from+'&to='+to+'&pType='+paymentType+'&username='+username, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        const responseObj = await response.blob();
        document.getElementById("loading-icon").style.display = "none";
        download(responseObj, "Sales Report on "+new Date().toISOString().slice(0, 10)+".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    }

    const getSalesReportPDF = async () => {
        const username = localStorage.getItem('user');
        document.getElementById("loading-icon").style.display = "block";
        let from = startDate.toISOString().slice(0, 10);
        let to = endDate.toISOString().slice(0, 10);
        const response = await fetch('/sales/download/report/pdf?from='+from+'&to='+to+'&pType='+paymentType+'&username='+username, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        const responseObj = await response.blob();
        const fileURL = URL.createObjectURL(responseObj);//Open the URL on new Window
        document.getElementById("loading-icon").style.display = "none";
        // download(fileURL);
        // window.open(fileURL);

        // create <a> tag dinamically
        var fileLink = document.createElement('a');
        fileLink.href = fileURL;

        // it forces the name of the downloaded file
        fileLink.download = "Sales Report on "+new Date().toISOString().slice(0, 10);

        // triggers the click event
        fileLink.click();
    }

    const logout = async () => {
        localStorage.removeItem("jt");
        const response = await fetch('/users/logout?username='+localStorage.getItem("user"), {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        localStorage.removeItem("user")
    }

    return (
        <div className="content">
            <div className="heading-bar"><div style={{ paddingTop: '10px' }}>Sales <Link to="/login" onClick={e => logout()}><IoIosLogOut id="logout-btn" /></Link></div></div>
            <div>
                <Button id="customize-btn" aria-controls="filter-items" aria-haspopup="true" onClick={e => setAnchorEl(e.currentTarget)} variant="contained" color="primary" style={{ marginLeft: '2%', marginTop: '1%' }}>
                    Customize Filters
                </Button>
                <Menu
                    id="filter-items"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={e => setAnchorEl(null)}
                >
                    <MenuItem>Payment Type<Checkbox checked={ptCheck} onChange={e => { if (ptCheck === false) { setPtCheck(true); } else { setPtCheck(false) } }} /></MenuItem>
                </Menu>
            </div>
            <div id="dashboard-content">
                <div id="filter-content">
                    <div className="filter-sub-content">
                        <FormControl className="filters">
                            <InputLabel id="payment-type-label" className="labels">Payment Type</InputLabel>
                            <Select labelId="payment-type-label" value={paymentType} onChange={e => setPaymentType(e.target.value)} className="select">
                                <MenuItem selected={paymentType === ""} value="">ALL</MenuItem>
                                <MenuItem selected={paymentType === "CASH"} value="CASH">Cash</MenuItem>
                                <MenuItem selected={paymentType === "CARD"} value="CARD">Card</MenuItem>
                                <MenuItem selected={paymentType === "CASH AND CARD"} value="CASH AND CARD">Cash & Card</MenuItem>
                            </Select>
                        </FormControl>
                    </div>
                    <table id="date-range">
                        <tr>
                            <td>From</td>
                            <td>
                                <DatePicker
                                    selected={startDate}
                                    onChange={date => setStartDate(date)}
                                    selectsStart
                                    startDate={startDate}
                                    endDate={endDate}
                                    className="date-elements"
                                />
                            </td>
                            <td>To</td>
                            <td>
                                <DatePicker
                                    selected={endDate}
                                    onChange={date => setEndDate(date)}
                                    selectsEnd
                                    startDate={startDate}
                                    endDate={endDate}
                                    minDate={startDate}
                                    className="date-elements"
                                />
                            </td>
                        </tr>
                    </table>
                </div>
                <Dashboard lineGraph={lineGraph} />
            </div>
            
            <div id="sales">
                <div id="download-btns">
                    <button type="button" onClick={e => chooseReportType("sales")} className="download-btns">Download Report</button>
                </div>
                <div id="tbl-container"><SalesTable rows={salesTblRows} /></div>
                <div id="summary-tbl-container"><SummaryTable rows={summaryTblRows} /></div>
            </div>
            <Modal style={ modalStyle } id="download-report-type-modal" isOpen={showModal} closeOnDocumentClick onRequestClose={e => closeModal()}>
                {modalContent}
            </Modal>
        </div>
    );
}

export default Sales;
import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import './style/app.css'

import Login from './components/login/login';
import Register from './components/login/register';
import Nav from './components/navigation/nav';
import MainDashboard from './components/dashboard/mainDashboard';
import Sales from './components/report/sales';
import SalesItem from './components/report/salesItem';
import Purchasing from './components/report/purchasing';
import Stock from './components/report/stock';
import Discount from './components/report/discount';
import Dashboard from './components/dashboard/dashboard';
 
function App() {
  const navFocusDashboard = {"route": "dashboard"};
  const navFocusSales = {"route": "sales"};
  const navFocusSalesItem = {"route": "sales-item"};
  const navFocusPurchasing = {"route": "purchasing"};
  const navFocusStock = {"route": "stock"};
  const navFocusDiscount = {"route": "discount"};

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Login />
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/register">
          <Register />
        </Route>
        <Route exact path="/dashboard">
          <Nav {...navFocusDashboard} />
          <MainDashboard />
        </Route>
        <Route exact path="/sales">
          <Nav {...navFocusSales} />
          <Sales />
        </Route>
        <Route exact path="/sales-item">
          <Nav {...navFocusSalesItem} />
          <SalesItem />
        </Route>
        <Route exact path="/purchasing">
          <Nav {...navFocusPurchasing} />
          <Purchasing />
        </Route>
        <Route exact path="/stock">
          <Nav {...navFocusStock} />
          <Stock />
        </Route>
        <Route exact path="/discount">
          <Nav {...navFocusDiscount} />
          <Discount />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
